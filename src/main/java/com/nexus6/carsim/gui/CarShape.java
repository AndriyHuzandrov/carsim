package com.nexus6.carsim.gui;

import com.nexus6.carsim.model.CarConfig;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;


public class CarShape extends JComponent implements Runnable {
  private CarConfig engine;
  private int coorX;
  private int currSpeed;
  private long timeBefore;

  public CarShape() {
    Thread anim = new Thread(this);
    timeBefore = System.currentTimeMillis();
    anim.start();
  }
  CarShape(CarConfig eng) {
    Thread anim = new Thread(this);
    engine = eng;
    timeBefore = System.currentTimeMillis();
    anim.start();
  }
  public void paint(Graphics g) {
    g.setColor(Color.BLACK);
    g.drawString(currSpeed  + " km/h", coorX , 30);
    g.setColor(Color.BLUE);
    g.fillRoundRect(coorX + 20, 45, 40, 25, 10, 12);
    g.fillRoundRect(coorX , 50, 90, 20, 5, 5);
    g.setColor(Color.BLACK);
    g.fillOval(coorX + 15, 60, 15, 15);
    g.fillOval(coorX + 60, 60, 15, 15);
  }
  public void run() {
    while (coorX < 1000) {
      coorX = engine.getParams(timeBefore).getPosition();
      currSpeed = engine.getParams(timeBefore).getSpeed();
      repaint();
      try {
        Thread.sleep(5);
      } catch (InterruptedException exc) {
        System.out.println(UIConst.INTERPTED_ERROR_MSG);
      }
    }
  }
}

