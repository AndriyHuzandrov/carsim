package com.nexus6.carsim.gui;

import com.nexus6.carsim.model.ServiceCenter;
import com.nexus6.carsim.observ.TestDrive;
import com.nexus6.carsim.observ.Updatable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainMenu {

  private String[] menuItems;
  private BufferedReader br;

  public MainMenu() {
    br = new BufferedReader(new InputStreamReader(System.in));
    menuItems = new String[UIConst.LAST_ITEM];
    menuItems[0] = "Upgrade with race tyres";
    menuItems[1] = "Upgrade with carbon body";
    menuItems[2] = "Upgrade with turbo";
    menuItems[3] = "Hang mascot on rare view mirror";
  }

  private void showPause(BufferedReader b, ServiceCenter sc) {
    System.out.print(UIConst.PAUSE_MSG);
    try {
      b.readLine();
      getChoice(sc);
    } catch (IOException e) {
      System.out.println(UIConst.INPUT_ERROR);
    }
  }

  private void showMenu() {
    int menuCount = 0;
    System.out.println(UIConst.WLC_MSG);
    for (String itm : menuItems) {
      menuCount++;
      System.out.printf("%2d. %s%n", menuCount, itm);
    }
    System.out.print(UIConst.EXIT_MSG);
  }

  public void getChoice(ServiceCenter sCenter) {
    showMenu();
    int choice = -1;
    try {
      String input = br.readLine();
      if (!input.matches("\\d")) {
        throw new IllegalArgumentException();
      } else if (Integer.valueOf(input) < UIConst.FIRST_ITEM
          || Integer.valueOf(input) > UIConst.LAST_ITEM) {
        throw new IllegalArgumentException();
      } else {
        choice = Integer.valueOf(input);
      }
    } catch (IOException e) {
      System.out.println(UIConst.INPUT_ERROR);
    } catch (IllegalArgumentException exc) {
      System.out.println(UIConst.WRONG_MENU_ITEM_MSG);
      getChoice(sCenter);
    }
    execChoice(choice, sCenter);
  }

  private void execChoice(int choice, ServiceCenter sCenter) {
    switch (choice) {
      case 0:
        System.exit(0);
        break;
      case 1:
        applyMenuChoice(new TestDrive(), sCenter, 1);
        break;
      case 2:
        applyMenuChoice(new TestDrive(), sCenter, 2);
        break;
      case 3:
        applyMenuChoice(new TestDrive(), sCenter, 3);
        break;
      case 4:
        System.out.println(UIConst.MASCOT_MSG);
        showPause(br, sCenter);
        break;
      default:
        break;
    }
  }
  private void applyMenuChoice(Updatable obs, ServiceCenter sc, int choice) {
    sc.registerObs(obs);
    sc.notifyObs(choice);
    sc.removeObs(obs);
    showPause(br, sc);
  }
}
