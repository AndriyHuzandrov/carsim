package com.nexus6.carsim.gui;

class UIConst {
  static final String MASCOT_MSG = "\nSorry, but this option available" +
                                    " for Aston Martin PREMIUM customers only.";
  static final String WRONG_MENU_ITEM_MSG = "Wrong menu item";
  static final String INPUT_ERROR = "Reading error";
  static final int FIRST_ITEM = 0;
  static final int LAST_ITEM = 4;
  static final int X_FRAME_SIZE = 1200;
  static final int Y_FRAME_SIZE = 100;
  static final int X_WINDOW_POS = 100;
  static final int Y_WINDOW_POS = 200;
  static final int WINDOW_SHIFT = 150;
  static final String WLC_MSG = "\nWELCOME TO ASTON MARTIN CUSTOMER SERVICE\n";
  static final String EXIT_MSG = "\nMake choice or 0 to exit the program : ";
  static final String PAUSE_MSG = "\nHit ENTER key to continue ";
  static final String INTERPTED_ERROR_MSG = "Error while running thread";

  private UIConst() {}
}
