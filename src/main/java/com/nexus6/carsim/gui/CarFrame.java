package com.nexus6.carsim.gui;


import com.nexus6.carsim.model.CarConfig;
import java.awt.Dimension;
import javax.swing.JFrame;

public class CarFrame extends JFrame {

  public CarFrame() {
    super("Factory configuration");
    CarShape carModel = new CarShape(new CarConfig());
    carModel.setPreferredSize(new Dimension(UIConst.X_FRAME_SIZE, UIConst.Y_FRAME_SIZE));
    add(carModel);
    pack();
    setVisible(true);
    setLocation(UIConst.X_WINDOW_POS, UIConst.Y_WINDOW_POS);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  }

  public CarFrame(CarConfig eConf) {
    super(eConf.getUpgName());
    CarShape carModel = new CarShape(eConf);
    carModel.setPreferredSize(new Dimension(UIConst.X_FRAME_SIZE, UIConst.Y_FRAME_SIZE));
    add(carModel);
    pack();
    setVisible(true);
    setLocation(UIConst.X_WINDOW_POS, UIConst.Y_WINDOW_POS + UIConst.WINDOW_SHIFT);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  }
}
