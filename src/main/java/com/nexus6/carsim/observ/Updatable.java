package com.nexus6.carsim.observ;

import com.nexus6.carsim.model.CarConfig;

public interface Updatable {
  <T extends CarConfig> void update(T cc);
}
