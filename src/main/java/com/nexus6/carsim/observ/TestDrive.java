package com.nexus6.carsim.observ;

import com.nexus6.carsim.gui.CarFrame;
import com.nexus6.carsim.model.CarConfig;
import javax.swing.SwingUtilities;


public class TestDrive implements Updatable {

  public void update(CarConfig egn) {
    this.update();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        new CarFrame(egn);
      }
    });
    egn.getSummary();
  }

  public void update() {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        new CarFrame();
      }
    });

  }
}
