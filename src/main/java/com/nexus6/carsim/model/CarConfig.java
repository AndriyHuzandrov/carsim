package com.nexus6.carsim.model;


public class CarConfig {
  private int configID;
  private String modelName = "Aston Martin";
  private int acceleration = 3;
  private int MAX_SPEED = 200;
  private int upgCost;
  private String upgName = "Basic model";

  public CarConfig() {}

  public CarConfig(int id, int acc, int uCost,String uName) {
    configID = id;
    acceleration = acc;
    upgCost = uCost;
    MAX_SPEED  += acceleration*10;
    upgName = uName;
  }

  public class engineData{
    int x;
    int s;
    engineData(int coord, int speed) {
      x = coord;
      s = speed;
    }
    public int getPosition() {
      return x;
    }

    public int getSpeed() {
      return s;
    }
  }

  int getConfigID() {
    return configID;
  }


  public String getUpgName() {
    return upgName;
  }

  String getModelName() {
    return modelName;
  }

  int getUpgCost() {
    return upgCost;
  }

  public void getSummary() {
    String pattern = "%n%nModel: %s%nUpgrade: %s%nUpgrade Cost: %d pounds%n";
    System.out.printf(pattern, getModelName(), getUpgName(), getUpgCost());
  }

  public engineData getParams(long cTime) {
    long currTime;
    int speedFactor = 150;
    int distanceFactor = 2000;
    int currSpeed;
    int coorX;
    currTime = (System.currentTimeMillis()-cTime);
    currSpeed = (int) (this.acceleration * currTime)/speedFactor;
    if (currSpeed >= this.MAX_SPEED) {
      currSpeed =  this.MAX_SPEED ;
    }
    coorX = (int) (currSpeed * currTime)/distanceFactor;
    return new CarConfig.engineData(coorX, currSpeed);
  }
}
