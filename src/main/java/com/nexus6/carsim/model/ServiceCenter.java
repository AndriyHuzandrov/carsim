package com.nexus6.carsim.model;

import com.nexus6.carsim.observ.Updatable;
import java.util.ArrayList;

public class ServiceCenter {

  private ArrayList<CarConfig> cofigList = new ArrayList<>();
  private ArrayList<Updatable> observers = new ArrayList<>();

  public ServiceCenter() {
    cofigList.add(new CarConfig(3, 7, 5780, "Turbo charger"));
    cofigList.add(new CarConfig(2, 5, 3350, "Carbon body"));
    cofigList.add(new CarConfig(1, 4, 1460, "Race tyres"));

  }

  public void registerObs(Updatable obs) {
    observers.add(obs);
  }

  public void removeObs(Updatable obs) {
    int ind = observers.indexOf(obs);
    if (ind >= 0) {
      observers.remove(ind);
    }
  }

  public void notifyObs(int choice) {
    int confNeeded = 0;
    for (CarConfig c : cofigList) {
      if (c.getConfigID() == choice) {
        confNeeded = cofigList.indexOf(c);
      }
    }
    for (Updatable o : observers) {
      o.update(cofigList.get(confNeeded));
    }
  }
}
